# Rio Demo Environment

```bash
vagrant up
k3sup install --ip=172.22.102.101 --user=root --k3s-extra-args '--flannel-iface eth1' --k3s-version=v0.10.2
set -x KUBECONFIG (pwd)/kubeconfig
sudo curl -sfL https://github.com/rancher/rio/releases/download/v0.6.0-rc1/rio-darwin-amd64 -o /usr/local/bin/rio
sudo chmod 755 /usr/local/bin/rio
rio install

```
